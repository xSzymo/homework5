package com.homework.lib;

import com.homework.lib.model.dto.Inventory;
import com.homework.lib.model.dto.Product;
import com.homework.lib.model.dto.User;
import com.homework.lib.repository.InventoryRepository;
import com.homework.lib.repository.RentalRepository;
import com.homework.lib.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;

@SpringBootApplication
public class HomeworkApp {
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RentalRepository rentalRepository;

    public static void main(String[] args) {
        SpringApplication.run(HomeworkApp.class, args);
    }

    @PostConstruct
    @Transactional
    private void addSampleData() {
        Inventory inventory = new Inventory();
        inventory.setLocation("loc");
        inventory.setName("loc");
        inventory.setProducts(new ArrayList<>());
        inventoryRepository.save(inventory);

        Product product = new Product();
        product.setDescription("desc");
        product.setQuantity(3);
        product.setPrice(5);
        product.setRentals(new ArrayList<>());
        product.setInventory(inventory);

        inventory.getProducts().add(product);
        inventoryRepository.save(inventory);

        User user = new User();
        user.setName("user");
        user.setRentals(new ArrayList<>());
        userRepository.save(user);
    }
}
