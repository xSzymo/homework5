package com.homework.lib.service.interfaces;

public interface CheckoutService<E, V> {
    E checkoutItem(V v);
}
