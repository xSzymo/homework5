package com.homework.lib.service.interfaces;

import java.util.List;

public interface Tracker<V, S> {
    List<V> getAllNotCheckoutItems(long id);

    List<V> getUsersCheckoutItems(long id);

    List<S> getUsersWhoCheckoutItems(long id);
}
