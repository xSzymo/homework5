package com.homework.lib.service.interfaces;

public interface ReturnService<E, V> {
    E returnItem(V v);
}
