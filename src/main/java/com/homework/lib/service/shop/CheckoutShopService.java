package com.homework.lib.service.shop;

import com.homework.lib.model.dto.Product;
import com.homework.lib.model.web.CheckoutShopItem;
import com.homework.lib.repository.ProductRepository;
import com.homework.lib.service.exception.NotFoundRuntimeException;
import com.homework.lib.service.interfaces.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckoutShopService implements CheckoutService<Product, CheckoutShopItem> {
    private final ProductRepository productRepository;

    @Autowired
    public CheckoutShopService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product checkoutItem(CheckoutShopItem checkout) {
        Product product = productRepository.findById(checkout.getProductId()).orElseThrow(NotFoundRuntimeException::new);
        if (product.getQuantity() < checkout.getQuantity())
            throw new RuntimeException("Not enough items");

        product.subtractQuantity(checkout.getQuantity());
        return productRepository.save(product);
    }
}
