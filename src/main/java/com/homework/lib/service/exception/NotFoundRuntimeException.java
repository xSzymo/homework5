package com.homework.lib.service.exception;

public class NotFoundRuntimeException extends RuntimeException {

    public NotFoundRuntimeException() {
        super("Not found");
    }
}
