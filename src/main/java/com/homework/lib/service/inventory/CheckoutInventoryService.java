package com.homework.lib.service.inventory;

import com.homework.lib.model.dto.Product;
import com.homework.lib.model.dto.Rental;
import com.homework.lib.model.dto.User;
import com.homework.lib.model.web.CheckoutItem;
import com.homework.lib.repository.ProductRepository;
import com.homework.lib.repository.RentalRepository;
import com.homework.lib.repository.UserRepository;
import com.homework.lib.service.exception.NotFoundRuntimeException;
import com.homework.lib.service.interfaces.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Predicate;

@Service
public class CheckoutInventoryService implements CheckoutService<Product, CheckoutItem> {
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final RentalRepository rentalRepository;

    @Autowired
    public CheckoutInventoryService(ProductRepository productRepository, UserRepository userRepository, RentalRepository rentalRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.rentalRepository = rentalRepository;
    }

    @Override
    public Product checkoutItem(CheckoutItem checkout) {
        User user = userRepository.findById(checkout.getUserId()).orElseThrow(NotFoundRuntimeException::new);
        Product product = productRepository.findById(checkout.getProductId()).orElseThrow(NotFoundRuntimeException::new);
        if (product.getQuantity() < checkout.getQuantity())
            throw new RuntimeException("Not enough items");

        product.subtractQuantity(checkout.getQuantity());
        Product updatedProduct = productRepository.save(product);

        Optional<Rental> productRental = product.getRentals().stream().filter(getRentalPredicate(checkout)).findFirst();
        Optional<Rental> userRental = user.getRentals().stream().filter(getRentalPredicate(checkout)).findFirst();
        if (productRental.isPresent() && userRental.isPresent()) {
            return addQuantityToExistingRentals(checkout, user, product, updatedProduct, productRental.get(), userRental.get());
        }
        return createNewRentals(checkout, user, product, updatedProduct);
    }


    private Product createNewRentals(CheckoutItem checkout, User user, Product product, Product updatedProduct) {
        Rental rental = new Rental();
        rental.setQuantity(checkout.getQuantity());
        rental.setProduct(updatedProduct);
        rental.setUser(user);
        Rental newRental = rentalRepository.save(rental);

        user.getRentals().add(newRental);
        userRepository.save(user);
        updatedProduct.getRentals().add(rental);
        productRepository.save(updatedProduct);

        return product;
    }

    private Product addQuantityToExistingRentals(CheckoutItem checkout, User user, Product product, Product updatedProduct, Rental productRental, Rental userRental) {
        int quantity = productRental.getQuantity();
        productRental.setQuantity(quantity + checkout.quantity);
        userRental.setQuantity(quantity + checkout.quantity);
        userRepository.save(user);
        productRepository.save(updatedProduct);
        return product;
    }

    private Predicate<Rental> getRentalPredicate(CheckoutItem returnItem) {
        return user -> user.getUser().getId().equals(returnItem.getUserId()) && user.getProduct().getId().equals(returnItem.getProductId());
    }
}
