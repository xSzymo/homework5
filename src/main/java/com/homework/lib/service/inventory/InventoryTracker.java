package com.homework.lib.service.inventory;

import com.homework.lib.model.dto.Rental;
import com.homework.lib.model.dto.User;
import com.homework.lib.model.web.ProductWeb;
import com.homework.lib.repository.InventoryRepository;
import com.homework.lib.repository.UserRepository;
import com.homework.lib.service.exception.NotFoundRuntimeException;
import com.homework.lib.service.interfaces.Tracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class InventoryTracker implements Tracker<ProductWeb, User> {
    private final UserRepository userRepository;
    private final InventoryRepository inventoryRepository;

    @Autowired
    public InventoryTracker(UserRepository userRepository, InventoryRepository inventoryRepository) {
        this.userRepository = userRepository;
        this.inventoryRepository = inventoryRepository;
    }

    @Override
    public List<ProductWeb> getAllNotCheckoutItems(long id) {
        return inventoryRepository.findById(id)
                .orElseThrow(RuntimeException::new)
                .getProducts().stream()
                .filter(product -> product.getQuantity() > 0)
                .map(ProductWeb::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductWeb> getUsersCheckoutItems(long id) {
        User user = userRepository.findById(id).orElseThrow(NotFoundRuntimeException::new);
        return getUniqueUserProducts(user.getRentals()).values()
                .stream()
                .map(this::mapToProductWeb)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> getUsersWhoCheckoutItems(long id) {
        return userRepository.findAll()
                .stream()
                .filter(user -> user.getRentals() != null && user.getRentals().size() > 0)
                .filter(userHasAtLeastOneItemCheckoutFromGivenInventory(id))
                .collect(Collectors.toList());
    }

    private HashMap<Long, Rental> getUniqueUserProducts(List<Rental> rentals) {
        HashMap<Long, Rental> map = new HashMap<>();
        for (Rental rental : rentals) {
            Long id = rental.getProduct().getId();
            Rental retrieved = map.get(id);
            if (retrieved != null)
                rental.setQuantity(retrieved.getQuantity() + rental.getQuantity());
            map.put(id, rental);
        }
        return map;
    }

    private ProductWeb mapToProductWeb(Rental rental) {
        return new ProductWeb(rental.getProduct(), rental.getProduct().getPrice() & rental.getQuantity(), rental.getQuantity());
    }

    private Predicate<User> userHasAtLeastOneItemCheckoutFromGivenInventory(Long inventoryId) {
        return user -> user.getRentals().stream()
                .anyMatch(rental -> rental.getProduct().getInventory().getId().equals(inventoryId));
    }
}
