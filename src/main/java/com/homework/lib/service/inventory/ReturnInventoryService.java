package com.homework.lib.service.inventory;

import com.homework.lib.model.dto.Product;
import com.homework.lib.model.dto.Rental;
import com.homework.lib.model.dto.User;
import com.homework.lib.model.web.ReturnItem;
import com.homework.lib.repository.ProductRepository;
import com.homework.lib.repository.RentalRepository;
import com.homework.lib.repository.UserRepository;
import com.homework.lib.service.exception.NotFoundRuntimeException;
import com.homework.lib.service.interfaces.ReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

@Service
public class ReturnInventoryService implements ReturnService<Product, ReturnItem> {
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final RentalRepository rentalRepository;

    @Autowired
    public ReturnInventoryService(ProductRepository productRepository, UserRepository userRepository, RentalRepository rentalRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.rentalRepository = rentalRepository;
    }

    @Override
    public Product returnItem(ReturnItem returnItem) {
        User user = userRepository.findById(returnItem.getUserId()).orElseThrow(NotFoundRuntimeException::new);
        Product product = productRepository.findById(returnItem.getProductId()).orElseThrow(NotFoundRuntimeException::new);
        Rental productRental = getMatchingRental(returnItem, product.getRentals());
        Rental userRental = getMatchingRental(returnItem, user.getRentals());

        if (!rentalHasQuantityOf(productRental, userRental.getQuantity()) && !rentalHasQuantityOf(userRental, returnItem.getQuantity()))
            throw new RuntimeException("Wrong item has been retrieved");
        if (productRental.getQuantity() < returnItem.getQuantity())
            throw new RuntimeException("Not enough items");

        return saveReturnedItem(returnItem, user, product, productRental, userRental);
    }

    private Product saveReturnedItem(ReturnItem returnItem, User user, Product product, Rental productRental, Rental userRental) {
        product.addQuantity(returnItem.getQuantity());

        if (rentalHasQuantityOf(productRental, returnItem.getQuantity())) {
            user.getRentals().remove(productRental);
            product.getRentals().remove(productRental);

            rentalRepository.deleteById(productRental.getId());
        } else {
            productRental.setQuantity(productRental.getQuantity() - returnItem.getQuantity());
            userRental.setQuantity(userRental.getQuantity() - returnItem.getQuantity());

            rentalRepository.save(userRental);
        }
        userRepository.save(user);
        productRepository.save(product);

        return null;
    }

    private Rental getMatchingRental(ReturnItem returnItem, List<Rental> rentals) {
        return rentals.stream()
                .filter(getRentalPredicate(returnItem))
                .findFirst()
                .orElseThrow(NotFoundRuntimeException::new);
    }

    private Predicate<Rental> getRentalPredicate(ReturnItem returnItem) {
        return rental -> rental.getUser().getId().equals(returnItem.getUserId()) && rental.getProduct().getId().equals(returnItem.getProductId());
    }

    private boolean rentalHasQuantityOf(Rental productRental, Integer quantity) {
        return productRental.getQuantity().equals(quantity);
    }
}

