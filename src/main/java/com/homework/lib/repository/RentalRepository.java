package com.homework.lib.repository;

import com.homework.lib.model.dto.Rental;
import com.homework.lib.model.dto.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentalRepository extends CrudRepository<Rental, Long> {
}
