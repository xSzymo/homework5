package com.homework.lib.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue
    private Long id;
    private Integer price;
    private Integer type;
    private Integer name;
    private String description;
    private String image;
    private String shelf;
    private Integer quantity;

    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Rental> rentals;

    @JsonIgnore
    @ManyToOne
    private Inventory inventory;

    public void subtractQuantity(int value) {
        quantity -= value;
    }

    public void addQuantity(int value) {
        quantity += value;
    }
}
