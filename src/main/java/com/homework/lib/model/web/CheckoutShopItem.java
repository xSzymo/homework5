package com.homework.lib.model.web;

import lombok.Data;

@Data
public class CheckoutShopItem {
    public long productId;
    public int quantity;
}