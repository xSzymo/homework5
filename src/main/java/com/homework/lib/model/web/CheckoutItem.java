package com.homework.lib.model.web;

import lombok.Data;

@Data
public class CheckoutItem {
    public long userId;
    public long productId;
    public int quantity;
}