package com.homework.lib.model.web;

import com.homework.lib.model.dto.Product;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductWeb {
    private Integer price;
    private Integer type;
    private Integer name;
    private String description;
    private String image;
    private String shelf;
    private Integer quantity;

    public ProductWeb(Product product) {
        this.price = product.getPrice();
        this.quantity = product.getQuantity();
        this.type = product.getType();
        this.name = product.getName();
        this.description = product.getDescription();
        this.image = product.getImage();
        this.shelf = product.getShelf();
    }

    public ProductWeb(Product product, int price, int quantity) {
        this.price = price;
        this.quantity = quantity;
        this.type = product.getType();
        this.name = product.getName();
        this.description = product.getDescription();
        this.image = product.getImage();
        this.shelf = product.getShelf();
    }
}