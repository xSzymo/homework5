package com.homework.lib.controller;

import com.homework.lib.model.dto.Product;
import com.homework.lib.model.dto.User;
import com.homework.lib.model.web.CheckoutItem;
import com.homework.lib.model.web.ProductWeb;
import com.homework.lib.model.web.ReturnItem;
import com.homework.lib.service.interfaces.CheckoutService;
import com.homework.lib.service.interfaces.ReturnService;
import com.homework.lib.service.interfaces.Tracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("inventory")
public class InventoryController {
    @Autowired
    private ReturnService<Product, ReturnItem> inventoryReturner;
    @Autowired
    private CheckoutService<Product, CheckoutItem> inventoryCheckout;
    @Autowired
    private Tracker<ProductWeb, User> inventoryTracker;

    @PostMapping("return")
    public ProductWeb returnItems(@RequestBody ReturnItem returnItem) {
        Product product = inventoryReturner.returnItem(returnItem);
        return null;
        //        return new ProductWeb(product, returnItem.getQuantity() * product.getPrice(), returnItem.getQuantity());
    }

    @PostMapping("checkout")
    public ProductWeb checkoutItem(@RequestBody CheckoutItem item) {
        Product product = inventoryCheckout.checkoutItem(item);
        return new ProductWeb(product, item.getQuantity() * product.getPrice(), item.getQuantity());
    }

    @GetMapping("products/{id}")
    public List<ProductWeb> getAllNotCheckoutItems(@PathVariable Long id) {
        return inventoryTracker.getAllNotCheckoutItems(id);
    }

    @GetMapping("users/{id}")
    public List<User> getUsersWhoCheckoutItems(@PathVariable Long id) {
        return inventoryTracker.getUsersWhoCheckoutItems(id);
    }

    @GetMapping("users/{id}/items")
    public List<ProductWeb> getUsersCheckoutItems(@PathVariable Long id) {
        return inventoryTracker.getUsersCheckoutItems(id);
    }
}
