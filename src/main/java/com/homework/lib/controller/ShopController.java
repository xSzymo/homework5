package com.homework.lib.controller;

import com.homework.lib.model.dto.Product;
import com.homework.lib.model.web.CheckoutShopItem;
import com.homework.lib.model.web.ProductWeb;
import com.homework.lib.service.interfaces.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("shop")
public class ShopController {
    @Autowired
    private CheckoutService<Product, CheckoutShopItem> shopCheckout;

    @PostMapping("buy")
    public ProductWeb checkoutItem(@RequestBody CheckoutShopItem item) {
        Product product = shopCheckout.checkoutItem(item);
        return new ProductWeb(product, item.getQuantity() * product.getPrice(), item.getQuantity());
    }
}
